from persistent.mapping import PersistentMapping
from persistent.list import PersistentList
from persistent import Persistent
import datetime
from slugify import slugify
import logging
log = logging.getLogger(__name__)
import uuid
from ZODB.blob import Blob
from .security import (SUPERUSER_GROUP, generate_ciphertext)
from pyramid.security import (
    Allow,
    Everyone,)
from BTrees.OOBTree import OOBTree, OOSet


class Blobish(object):
    '''
    Documents/Raw data in here.
    '''
    def __init__(self, content):
        self.content = Blob(content)


class Attributable(object):
    def __init__(self, **properties):
        self.attributes = PersistentMapping()
        for key, value in properties.items():
            self.attributes[key] = value


class Ownable(object):
    @property
    def __acl__(self):
        '''
        Add edit permissions for the creator and groups the creator
        belogs to.

        Default will inherit the acl of the parent, all the way to the
        root, unless someone places a (Deny, Everyone, ALL_PERMISSIONS)
        in acl.
        '''
        return [
            (Allow, self.owner, 'edit'),
        ] + [(Allow, group, 'edit') for group in self.groups]

    def __init__(self, owner):
        self.owner = owner
        if hasattr(self.owner, 'groups'):
            self.groups = owner.groups
        else:
            self.groups = []


class BasicData(object):
    '''
    Every object in the graph is a BasicData. Any BasicData
    must have a name, date created, date modified.
    '''
    def __init__(self, parent, name, created_at=None):
        self.__name__ = name
        if parent:
            self.__parent__ = parent
        self.modified_at = self.created_at = \
            created_at or datetime.datetime.utcnow()

    @property
    def name(self):
        return self.__name__

    @name.setter
    def name(self, val):
        self.__name__ = val

    def __repr__(self):
        return "{0}({1})".format(
            self.__class__.__name__, self.name)


class Folderish(object):
    def contents(self):
        for item in self:
            if isinstance(item, BasicData):
                yield item


class UserFolder(PersistentMapping, BasicData, Folderish):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, SUPERUSER_GROUP, 'edit')]

    def __init__(self, parent, name):
        BasicData.__init__(self, parent, name)
        PersistentMapping.__init__(self)


class Folder(PersistentMapping, BasicData, Attributable, Ownable, Folderish):
    '''
    A folder is a Tree that contains anything else.
    '''
    def __init__(self, parent, name, owner, created_at=None):
        PersistentMapping.__init__(self)
        BasicData.__init__(self, parent, name, created_at)
        Attributable.__init__(self)
        Ownable.__init__(self, owner)

    def __add__(self, member):
        assert isinstance(member, BasicData), "Not BasicData capable!"
        if member.name not in self:
            self[member.name] = member
        return self

    def __sub__(self, member):
        assert isinstance(member, BasicData), "Not BasicData capable!"
        if member.name in self:
            del self[member.name]
            return self
        raise ValueError("%s not in %s" % (member.name, self.name))


class RootFolder(OOBTree, Folderish):
    __parent__ = __name__ = None

    def __init__(self):
        OOBTree.__init__(self)
        self['users'] = UserFolder(self, 'users')

    def add_user(self, user):
        self['users'][user.name] = user

    def add_content(self, content):
        assert isinstance(content, BasicData), "Illegal content!"
        self[content.name] = content

    def find_user(self, username):
        try:
            u = self['users'][username]
        except KeyError:
            u = None
        return u

    @property
    def __acl__(self):
        return [(Allow, Everyone, 'view'),
                (Allow, SUPERUSER_GROUP, 'edit')]


class Blog(Folder):
    def __init__(self, parent, name, title, description, creator_user):
        Folder.__init__(self, parent, name, creator_user)
        self.title = title
        self.description = description
        self.attributes['offsite_links'] = PersistentList()
        self.attributes['about_content'] = "Place your content here."
        self.attributes['about_header'] = "About"

    def entries(self, range_start=None, range_end=None):
        for item in self.values():
            if isinstance(item, Entry):
                yield item

    def date_ranges(self):
        bins = set()
        for entry in self.entries():
            bins.add(entry.created_at.timetuple()[:2])
        return [datetime.datetime(
            *(x + (1, ))).strftime('%B %Y')
            for x in sorted(bins, reverse=True)]

    def list_entries(self, range_start=None, range_end=None):
        return sorted(
            self.entries(range_start, range_end),
            key=lambda e: e.created_at, reverse=True)

    def __repr__(self):
        return "Blog({0}, owner={1})".format(self.name, self.owner)

    def add_entry(self, entry):
        return self + entry


class Category(object):
    def __init__(self, name, description=None):
        self.name = name
        self.description = description or ''

    def __eq__(self, other):
        return self.name == other.name and self.description == other.description

    def __hash__(self):
        return hash((self.name, self.description))

    def __repr__(self):
        return "{0}({1}, {2})".format(
            self.__class__.__name__, self.name, self.description)


class Entry(BasicData, Ownable, Attributable, Persistent):
    def __init__(self, blog, title, subtitle, author, body, created_at=None):
        BasicData.__init__(self, blog, slugify(title), created_at)
        Attributable.__init__(
            self, title=title, subtitle=subtitle, body=body,
            categories=OOSet())
        Ownable.__init__(self, author)
        Persistent.__init__(self)

    def __cmp__(self, other):
        if isinstance(other, datetime.datetime):
            return cmp(self.created_at, other)
        raise NotImplementedError()

    def add_category(self, category):
        assert isinstance(category, Category), "Invalid category type"
        self.attributes['categories'].add(category)

    def __contains__(self, other):
        if isinstance(other, Category):
            return other in self.attributes['categories']
        return other in self.attributes

    def __repr__(self):
        return "{0}({1})".format(
            self.__class__.__name__,
            ', '.join(
                '{0}={1}'.format(x, getattr(self, x))
                for x in ('name', 'title')))


class User(BasicData, Persistent):
    def __init__(self, root, username, password, full_name,
                 additional_groups=None):
        BasicData.__init__(self, root, username)
        Persistent.__init__(self)
        self.full_name = full_name
        self.entries = []
        self.salt = uuid.uuid1().hex.encode('utf8')
        self.password = generate_ciphertext(password, self.salt)
        self.groups = OOSet(['group:%s' % self.name])
        if additional_groups:
            for g in additional_groups:
                self.groups.add(g)

    def authorize(self, candidate_password):
        if generate_ciphertext(
                candidate_password, self.salt) == self.password:
            return None
        raise ValueError("Not Authorized")


def acquire_database(zodb_root, database_name):
    try:
        db = zodb_root[database_name]
    except KeyError:
        raise KeyError("You need to run graphblogr_zodb and make ZODB db!")
    return db
