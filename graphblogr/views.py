from pyramid.renderers import get_renderer
from pyramid.view import (view_config, forbidden_view_config,)
from .models import Blog, RootFolder
from pyramid.httpexceptions import HTTPFound
from pyramid.url import resource_url

from pyramid.security import (
    remember,
    forget)
import logging
log = logging.getLogger(__name__)


@view_config(context=RootFolder, permission='view')
def view_root(context, request):
    return HTTPFound(
        location=resource_url(context, request, 'blog'))


@view_config(
    context=Blog, renderer='templates/blog.pt', permission='view')
def render_blog(context, request):
    entry = get_renderer('templates/entry.pt').implementation()
    return {
        'page': context, 'main': entry,
        'logged_in': request.authenticated_userid}


@view_config(
    context=Blog, request_method='POST', name="submit", renderer='json',
    permission='edit')
def submit_blog_content_change(request):
    target = content = None
    try:
        target = request.json_body['type']
        content = request.json_body['data']
    except ValueError:
        log.debug("Invalid JSON ({0})".format(request.body[:100]))
    else:
        if target in request.context.attributes:
            request.context.attributes[target] = content
            return {'result': 'success'}
        log.debug("Illegal target ({0})".format(target))
    return {'result': 'failure'}


@view_config(context=RootFolder, name='login',
             renderer='templates/login.pt')
@forbidden_view_config(renderer='templates/login.pt')
def login(request):
    login_url = request.resource_url(request.context, 'login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/'  # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        for user_account in request.context['users'].keys():
            if user_account == login:
                user_account = request.context['users'][user_account]
                break
        else:
            user_account = None
        try:
            user_account.authorize(password)
        except ValueError as e:
            message = str(e)
        else:
            headers = remember(request, login)
            return HTTPFound(location=came_from,
                             headers=headers)
    return {'message': message,
            'url': request.application_url + '/login',
            'came_from': came_from,
            'login': login,
            'password': password}


@view_config(context=RootFolder, name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location=request.resource_url(request.context),
                     headers=headers)
