import logging
import binascii
import hashlib

log = logging.getLogger(__name__)

SUPERUSER_GROUP = 'group:admin'


def get_potential_user_groups(user_id, request):
    user = request.root.find_user(user_id)
    if user:
        return user.groups
    log.error("Failed to find user {0} to match".format(user_id))
    return None


def generate_ciphertext(password, salt):
    if not isinstance(password, bytes):
        password = password.encode('utf8')
    return binascii.hexlify(
        hashlib.sha512(password+salt).digest())
