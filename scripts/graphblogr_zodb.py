import ZODB
import ZODB.FileStorage
import transaction
from graphblogr.models import User, RootFolder, Entry, Blog
from graphblogr.security import SUPERUSER_GROUP
import json
import datetime


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', metavar="ZODB_FILE", type=str)
    parser.add_argument(
        '-n', '--name', metavar="DATABASE_NAME", type=str, default='blogr')
    parser.add_argument(
        '-b', '--blog-id', metavar="BLOG_ID", type=str, default='blog',
        dest='blogid',
        help="id of the blog inside the resource root (default blog).")
    parser.add_argument(
        '-f', '--fake-data', metavar="DATA", type=str,
        help='JSON array of blog entries with the keys: title, '
             'subtitle, author, body, data (as a unix epoch)',
        dest="fakedata", default=None)
    args = parser.parse_args()

    storage = ZODB.FileStorage.FileStorage(args.filename)
    zodb = ZODB.DB(storage)
    conn = zodb.open()
    zodb_root = conn.root()
    db = zodb_root[args.name] = RootFolder()
    root = User(db, 'admin', 'admin', "Root Toor", [SUPERUSER_GROUP])
    db.add_user(root)
    transaction.commit()
    blog = Blog(
        db, args.blogid,
        '{0}\'s Blog'.format(root.full_name),
        'A Blog', root)
    db.add_content(
        blog)
    transaction.commit()
    if args.fakedata:
        with open(args.fakedata, 'rb') as fh:
            items = json.loads(fh.read().decode('utf8'))
            for entry in items:
                blog_entry = Entry(
                    blog, entry['title'], entry['subtitle'],
                    root, entry['body'],
                    datetime.datetime.utcfromtimestamp(entry['time']))
                blog.add_entry(blog_entry)
                transaction.commit()
        transaction.commit()
    conn.close()
    zodb.close()
    storage.close()
